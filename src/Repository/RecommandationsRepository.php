<?php

namespace App\Repository;

use App\Entity\Recommandations;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Recommandations|null find($id, $lockMode = null, $lockVersion = null)
 * @method Recommandations|null findOneBy(array $criteria, array $orderBy = null)
 * @method Recommandations[]    findAll()
 * @method Recommandations[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecommandationsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Recommandations::class);
    }

    public function findByCountRecommandation($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.films = :val')
            ->setParameter('val', $value)
            ->select('COUNT(r.id)')
            ->getQuery()
            ->getResult();
    }

    public function findByTotalNoteByFilm($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.films = :val')
            ->setParameter('val', $value)
            ->select('SUM(r.note) as note')
            ->getQuery()
            ->getResult();
    }
    // /**
    //  * @return Recommandations[] Returns an array of Recommandations objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Recommandations
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
