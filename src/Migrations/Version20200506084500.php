<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200506084500 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE films (id INT AUTO_INCREMENT NOT NULL, titre VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, realisateur VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, acteur VARCHAR(255) NOT NULL, duree VARCHAR(255) NOT NULL, image VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE membre (id INT AUTO_INCREMENT NOT NULL, users_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, age INT NOT NULL, UNIQUE INDEX UNIQ_F6B4FB2967B3B43D (users_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE recommandations (id INT AUTO_INCREMENT NOT NULL, films_id INT DEFAULT NULL, membres_id INT DEFAULT NULL, note INT NOT NULL, commentaire VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, auteur VARCHAR(255) NOT NULL, INDEX IDX_2E7C1FDB939610EE (films_id), INDEX IDX_2E7C1FDB71128C5C (membres_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, reset_token VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE membre ADD CONSTRAINT FK_F6B4FB2967B3B43D FOREIGN KEY (users_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE recommandations ADD CONSTRAINT FK_2E7C1FDB939610EE FOREIGN KEY (films_id) REFERENCES films (id)');
        $this->addSql('ALTER TABLE recommandations ADD CONSTRAINT FK_2E7C1FDB71128C5C FOREIGN KEY (membres_id) REFERENCES membre (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE recommandations DROP FOREIGN KEY FK_2E7C1FDB939610EE');
        $this->addSql('ALTER TABLE recommandations DROP FOREIGN KEY FK_2E7C1FDB71128C5C');
        $this->addSql('ALTER TABLE membre DROP FOREIGN KEY FK_F6B4FB2967B3B43D');
        $this->addSql('DROP TABLE films');
        $this->addSql('DROP TABLE membre');
        $this->addSql('DROP TABLE recommandations');
        $this->addSql('DROP TABLE user');
    }
}
