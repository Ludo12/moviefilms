<?php

namespace App\Controller;

use App\Entity\Membre;
use App\Entity\User;
use App\Form\UserType;
use App\Repository\MembreRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/users")
 */
class UserController extends AbstractController
{

    /**
     * @Route("/", name="user.index")
     * @param UserRepository $repository
     * @param MembreRepository $membreRepository
     * @return Response
     */
    public function index(UserRepository $repository, MembreRepository $membreRepository)
    {
        $u = $repository->findId();
        $roles = $membreRepository->findByUser($u);

        return $this->render('user/index.html.twig', [
            'roles' => $roles,
        ]);
    }

    /**
     * @Route("/{id}", name="user.delete", methods="DELETE")
     * @param Membre $membre
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Membre $membre, Request $request)
    {
        if ($this->isCsrfTokenValid('delete' . $membre->getId(), $request->get('_token'))) {
            $this->getDoctrine()->getManager()->remove($membre);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'User supprimé avec succès');
        }
        return $this->redirectToRoute('user.index');
    }

    /**
     * @Route("/edit/{id}", name="user.edit", methods="GET|POST")
     * @param User $user
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function edit(User $user, Request $request)
    {

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'User modifié avec succès');
            return $this->redirectToRoute('user.index');
        }
        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'formUser' => $form->createView()
        ]);
    }
}
