<?php

namespace App\Controller;

use App\Entity\Films;
use App\Entity\Membre;
use App\Entity\Recommandations;
use App\Entity\User;
use App\Form\RecommandationsType;
use App\Repository\FilmsRepository;
use App\Repository\RecommandationsRepository;
use DateTime;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccueilController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     * @param FilmsRepository $filmsRepository
     * @return Response
     */
    public function index(FilmsRepository $filmsRepository)
    {
        $films = $filmsRepository->orderByDate();
        return $this->render('accueil/index.html.twig', [
            'films' => $films,
        ]);
    }

    /**
     * @Route("/aporpos", name="apropos")
     * @param FilmsRepository $filmsRepository
     * @return Response
     */
    public function apropos(FilmsRepository $filmsRepository)
    {
        $films = $filmsRepository->orderByDate();
        return $this->render('apropos/index.html.twig', [
            'films' => $films,
        ]);
    }

    /**
     * @Route("/movie", name="movie.index")
     * @param FilmsRepository $filmsRepository
     * @return Response
     */
    public function home(FilmsRepository $filmsRepository)
    {
        $films = $filmsRepository->findAll();
        return $this->render('movie/index.html.twig', [
            'all_films' => $films,
        ]);
    }

    /**
     * @Route("/movie/{id}", name="movie.show")
     * @param Films $film
     * @param Request $request
     * @param RecommandationsRepository $repo
     * @param User $user
     * @return Response
     * @throws Exception
     */
    public
    function show(Films $film, Request $request, RecommandationsRepository $repo, User $user)
    {

        $total = $repo->findByTotalNoteByFilm($film->getId());
        $maxi = $repo->findByCountRecommandation($film->getId());
        if (intval($maxi) == 0) {
            $maxi = 1;
        }

        $moyenne = intval($total) / intval($maxi);

        $recommandations = new Recommandations();
        $form = $this->createForm(RecommandationsType::class, $recommandations);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $this->isGranted($user)) {
            $membre = $this->getDoctrine()->getRepository(Membre::class)->find($user);
            $recommandations->setCreatedAt(new DateTime())
                ->setMembres($membre)
                ->setFilms($film);

            $em = $this->getDoctrine()->getManager();
            $em->persist($recommandations);
            $em->flush();

            return $this->redirectToRoute('movie.show', ['id' => $film->getId()]);
        }
        return $this->render('movie/show.html.twig', [
            'movie' => $film,
            'moyenne' => $moyenne,
            'form' => $form->createView()
        ]);
    }
}
