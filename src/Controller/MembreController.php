<?php

namespace App\Controller;

use App\Entity\Membre;
use App\Repository\MembreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/profile/membre")
 */
class MembreController extends AbstractController
{
    /**
     * @Route("/", name="membre.index")
     */
    public function index()
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $monId = $this->getUser()->getId();
        $membre = $this->getDoctrine()->getRepository(Membre::class)->find($monId);
        $user = $this->getUser();

        return $this->render('membre/index.html.twig', [
            'membre' => $membre,
            'user' => $user
        ]);
    }
}
