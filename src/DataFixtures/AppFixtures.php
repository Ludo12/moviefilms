<?php

namespace App\DataFixtures;

use App\Entity\Films;
use App\Entity\Membre;
use App\Entity\Recommandations;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        for ($u = 0; $u < 20; $u++) {
            $user = new User();
            $membre = new Membre();
            $user->setEmail($faker->freeEmail)
                ->setRoles(['ROLE_USER'])
                ->setPassword($this->passwordEncoder->encodePassword($user, 'azertyui'));
            $membre->setNom($faker->lastName)
                ->setPrenom($faker->firstName)
                ->setAge($faker->numberBetween($min = 18, $max = 100))
                ->setUsers($user);
            $manager->persist($user);
            $manager->persist($membre);

            for ($i = 0; $i < 20; $i++) {
                $films = new Films();
                $content = $faker->firstName . " " . $faker->lastName;
                $films->setTitre($faker->sentence(4))
                    ->setDescription($faker->text())
                    ->setRealisateur($content)
                    ->setActeur($faker->lastName)
                    ->setDuree($faker->time('H:i:s', '12000'))
                    ->setCreatedAt($faker->dateTimeBetween('-3 years'))
                    ->setImage($faker->imageUrl(400, 200, 'people'));

                $manager->persist($films);

                for ($j = 1; $j <= mt_rand(4, 6); $j++) {
                    $recommandation = new Recommandations();

                    //On fait un commentaire entre la date du jour et la date de creation de l'article
                    $now = new \DateTime();//date du jour
                    //on récupère la différence entre les 2 objets Datetime
                    $interval = $now->diff($films->getCreatedAt());
                    //on récupère le nombre de jour
                    $days = $interval->days;
                    $minim = '-' . $days . 'days'; //example: -100 days

                    $recommandation->setAuteur($faker->firstName())
                        ->setCommentaire($faker->paragraph(1))
                        ->setNote($faker->numberBetween($min = 0, $max = 5))
                        ->setCreatedAt($faker->dateTimeBetween($minim))
                        ->setFilms($films)
                        ->setMembres($membre);

                    $manager->persist($recommandation);
                }
            }
        }
        $manager->flush();
    }
}
